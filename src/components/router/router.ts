import { createRouter, createWebHashHistory } from 'vue-router';
import Main from '../pages/Main.vue';
import Display from '../pages/Display.vue';
import ErrorPage from '../pages/ErrorPage.vue';

const routes = [
	{
		path: '/',
		component: Main
	},
	{
		path: '/display',
		component: Display
	},
	{
		path: '/:pathMatch(.*)*',
		component: ErrorPage
	}
];

const router = createRouter({
	routes,
	history: createWebHashHistory()
});

export default router;