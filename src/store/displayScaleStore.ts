import { reactive } from 'vue';

const useWidth = window.innerWidth;

interface DisplayScaleStore {
	useScale: number;
	changeUseScale: (value: number) => void;
}

function startScale() {
	if (useWidth < 700) {
		return 0.5;
	} else if (useWidth < 780) {
		return 0.7;
	} else if (useWidth < 1000) {
		return 0.8;
	} else return 1;
	
}

export const displayScaleStore = reactive<DisplayScaleStore>({
	useScale: startScale(),
	changeUseScale(value) {
		this.useScale = value;
	}
});