import { reactive } from 'vue';

const useWidth = window.innerWidth;
const useHeight = window.innerHeight;

interface Style{
  left: string
  top: string
  transform: string
}

interface Display {
	id: number;
	title: string;
	x: number;
	y: number;
	className: string;
	isHide: boolean;
	scale: number;
	rotate: boolean | null;
	getStyle: () => Style;
}

interface MyStore {
	displays: Display[];
	useUrl: string;
	changeUseUrl: (value: string) => void;
	moveDisplay: (index: number, x: number, y: number) => void;
	hideDisplay: (id: number) => void;
	changeScale: (id: number, scale: number) => void;
	rotateDisplay: (id: number) => void;
}
type ClassName = 'mobile' | 'tablet' | 'laptop' | 'desktop';

class SampleDisplay {
	id: number;
	title: string;
	x: number;
	y: number;
	className: ClassName;
	isHide: boolean;
	scale: number;
	rotate: boolean | null;

	constructor(id: number, title: string, x: number, y: number, className: ClassName) {
		this.id = id;
		this.title = title;
		this.x = x;
		this.y = y;
		this.className = className;
		this.isHide = false;
    this.scale = 1;
		if (className === 'mobile' || className === 'tablet') {
			this.rotate = false;
		}else this.rotate = null;
	}
	getStyle() {
		return {
			left: `${this.x}px`,
			top: `${this.y}px`,
			transform: this.createTransform()
		};
	}
	private createTransform() {
		switch (this.className) {
			case 'desktop':
				return `scale(${this.scale}) rotate3d(0, 1, 0, -3deg)`;
			case 'tablet':
				return this.scale <= 1 ? `scale(${this.scale}) rotate3d(0, 1, 0, 13deg)` : `scale(${this.scale})`;
			case 'laptop':
				return `scale(${this.scale}) rotate3d(0, 1, 0, -3deg)`;
			default:
				return `scale(${this.scale})`;
		}
	}

}

function startX(name: string) {
	const usewith = useWidth < 1800 ? useWidth : 1800;
	switch (name) {
		case 'desktop':
			return usewith / 2 - 280;
		case 'laptop':
			return usewith / 2;
		case 'tablet':
			return usewith / 2 - 350;
		case 'mobile':
			return usewith / 2 - 200;
		default:
			return usewith / 2;
	}
}
function startY(name: string) {
	const useheight = useHeight < 1000 ? useHeight : 1000;
	switch (name) {
		case 'desktop':
			return useheight / 2 - 280;
		case 'laptop':
			return useheight / 2 + 5;
		case 'tablet':
			return useheight / 2-80;
		case 'mobile':
			return useheight / 2 + 80;
		default:
			return useheight / 2;
	}
}

const initialDisplays: Display[] = [
	new SampleDisplay(1, 'смартфон', startX('mobile'), startY('mobile'), 'mobile'),
	new SampleDisplay(2, 'планшет', startX('tablet'), startY('tablet'), 'tablet'),
	new SampleDisplay(3, 'ноутбук', startX('laptop'), startY('laptop'), 'laptop'),
	new SampleDisplay(4, 'монітор', startX('desktop'), startY('desktop'), 'desktop')
];

export const myStore = reactive<MyStore>({
	displays: [...initialDisplays],

	useUrl: '',

	changeUseUrl(value) {
		this.useUrl = value;
	},

	moveDisplay(index, x, y) {
		this.displays[index].x = x;
		this.displays[index].y = y;
	},

	hideDisplay(id) {
		const index = this.displays.findIndex(el => el.id === id);
		if (index !== -1) {
			this.displays[index].isHide = !this.displays[index].isHide;
		}
	},

	changeScale(id, scale) {
		const index = this.displays.findIndex(el => el.id === id);
		if (index !== -1) {
			this.displays[index].scale = scale;
		}
	},

	rotateDisplay(id) {
		const index = this.displays.findIndex(el => el.id === id);
		if (index !== -1) {
			if (this.displays[index].rotate === null) {
				return;
			} else {
				this.displays[index].rotate = !this.displays[index].rotate;
			}
		} else {
			console.warn(`Rotate operation failed. Display with id ${id} not found.`);
		}
	}
});
