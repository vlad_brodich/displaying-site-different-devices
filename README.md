# Курсова робота Vue 3 Влад Бродич
Переглянути: https://displaying-site-different-devices.netlify.app/
## Проект: Відображення сайту на різних пристроях.

1. Дозволяє переглянути проект на різних пристроях.
2. Налаштувати адаптивність проекту.
3. Створити презентацію проекту для портфоліо.

## Проект створено за допомгою:
Vue 3, TypeScript, Валідація даних - Zod, Слайдер - vue3-carousel, збиральник - Vite